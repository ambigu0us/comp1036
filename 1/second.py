#!/usr/bin/python3
# second.py
# Asks for name and greets
# Author:     TB
# Created on: 2013-01-30
# Version:    1.0
hi = "Hello "
name = input('Please enter your name: ')
end = ", it's always a pleasure to see you."
print(hi + name + end)
