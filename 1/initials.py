#!/usr/bin/python3
# initial.py
# Prints my initials and name
# Author:     TB
# Created on: 2013-01-30
# Version:    1.0

initials = """##### #    # #####
  #   #    # #    #
  #   #    # #####
  #   # ## # #    #
  #   ##  ## #    #
  #   #    # #####

My name is Thomas William Byrne"""
print(initials)
