#!/usr/bin/python3
# third.py
# counts eggs before hatching
# Author:     TB
# Created on: 2013-01-30
# Version:    1.0
import sys
eggs = input('How many eggs are there in the basket? ')
try:
	eggs = int(eggs)
except ValueError:
	print("Please enter a integer number of eggs!")
	sys.exit(0)

if eggs <= 0:
	print("What are you doing with those negative eggs? Put them back at once!")
	sys.exit(0)

boxes = int(eggs/6)
leftover = eggs%6

if eggs <= 6 and eggs > 0:
	boxgrammar = "box"
	boxes = 1 #Not sure if I'm doing this right, it works, but seems a bit silly
	leftover = 0
else:
	boxgrammar = "boxes"

print("You will need", boxes, boxgrammar, "and will have", leftover, "left over.")
