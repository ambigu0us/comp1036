#!/usr/bin/python3
# mathstest.py
# less simple maths test
# Author:     TB
# Created on: 2013-01-30
# Version:    1.0

import random

right = 0 #not actually needed

for i in range (10): #start a for loop where i goes from 0-9

	#create two random integers between 1-100 (inclusive)
	a = random.randint(1, 100)
	b = random.randint(1, 100)
	
	#format the question string
	question = "Question " + str(i+1) + ": What is " + str(a) + " + " + str(b) + "? "
	
	#get user answer and check sanity
	while True: #start an infinite loop for input
		answer = input(question)
		try:
			answer = int(answer)
			break #if the input is integer break out of the loop
		except ValueError:
	        	print("Please enter a integer number!")
	
	#calculate real answer
	realanswer = a + b
	
	
	if answer == realanswer: #check if they got it right
		print( answer, "is correct!\n")
		right = right + 1
	else:
		print( answer, "is incorrect! The correct answer is", a + b, "\n")
	
print("Test completed, you got", right, "out of 10 questions correct.") #print end of test report


