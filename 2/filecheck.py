#!/usr/bin/python3
# filecheck.py
# checks if file exists
# Author:     TB
# Created on: 2013-02-20
# Version:    1.0

try:
	open(input("Enter filename for checking: "), 'r') #ask for input and try to open file
	print("File exists") # If the opening is successful
except IOError: # if there is an IO error (indicative of a non existent file)
	print("File doesn't exist") 

#alternatively os.path.isfile() is a quicker way to do it
