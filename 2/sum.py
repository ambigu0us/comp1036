#!/usr/bin/python3
# sum.py
# simple maths test
# Author:     TB
# Created on: 2013-01-30
# Version:    1.0

import random

#create two random integers between 1-100 (inclusive)
a = random.randint(1, 100)
b = random.randint(1, 100)

#format the question string
question = "What is " + str(a) + " + " + str(b) + "? "

#get user answer and check sanity
answer = input(question)
try:
        answer = int(answer)
except ValueError:
        print("Please enter a integer number!")

#calculate real answer
realanswer = a + b


if answer == realanswer: #check if they got it right
	print( answer, "is correct!")
else:
	print( answer, "is incorrect! The correct answer is", a + b)



