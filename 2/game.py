#!/usr/bin/python3
# game.py
# Shall we play a game?
# Author:     TB
# Created on: 2013-02-20
# Version:    1.0

import random

#function to get a santised guess from user (integer between 1-100 inclusive)
def guess(): 

	while True: #start an loop for input
		guess = input("Guess what number I am thinking of? (1-100) ") #get user input
		try:
			guess = int(guess) #this will error if not integer
			if guess <=100 and guess >= 1: #check 1<guess<100
				break #break out of while loop if true
			else:
				#error message for out of bounds number
				print("Number is not between 1-100 (inclusive), try again.") 
		except ValueError:
			#error message for non integer number
			print("Number is not integer, try again.")

	return guess #return the guess


#generate a random number 1-100 that is not 25, 50 or 75
randNum = 0

# fill an array with the numbers we don't want
badNumbers=[0,25,50,75] 

#this loop will pick a new number if the number is one of the badnumbers
while randNum in badNumbers: 
	randNum = random.randint(1, 100) # random number between 1-100 (inclusive)

#print( randNum ) #just for debugging
guesses=[] #intialize an empty array for the guesses

#main loop
while True: 
	curguess = guess() #get a sanitised guess from the user
	if curguess in guesses: # check if it's already been guessed
		print("You've already guessed", curguess, ", guess again.") #print message and return to beginning of loop
	else: #if its a new guess
		guesses.append(curguess) # append the guess to the guess array
		if curguess > randNum: #if its too big
			print("The number I am thinking of is lower than", curguess)
		elif curguess < randNum: #if its too small
			print("The number I am thinking of is higher than", curguess)
		else: #just right!
			print("You guessed it! It took you", len(guesses), "guesses.") 
			break #break out of main loop and finish program
	
